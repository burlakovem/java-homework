package ru.burlakov.app.gson;

import java.util.Map;


public class GsonRequest {
    private String action;
    private Map<String, String> parameters;

    public GsonRequest(String action, Map<String, String> parameters){
        this.action = action;
        this.parameters = parameters;
    }
}
