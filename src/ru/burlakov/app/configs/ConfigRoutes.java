package ru.burlakov.app.configs;

import ru.burlakov.app.controllers.ControllerApiAuth;
import ru.burlakov.app.controllers.ControllerAuth;
import ru.burlakov.app.controllers.ControllerMain;
import ru.burlakov.core.Controller;

import java.util.HashMap;
import java.util.Map;


/**
 * Config for router
 */
public class ConfigRoutes {

    /**
     * Routes
     */
    private static Map<String, Controller> routes = new HashMap<>();

    {

        routes.put("/", new ControllerMain());

        routes.put("/auth/", new ControllerAuth());
        routes.put("/auth/registration/", new ControllerAuth("registration"));

        routes.put("/api/auth/", new ControllerApiAuth());

    }

    /**
     * Getting routes
     * @return
     */
    public static Map<String, Controller> getRoutes(){
        return routes;
    }

}
