package ru.burlakov.core.db;

import ru.burlakov.app.configs.ConfigDataBase;

import java.sql.*;
import java.util.Map;


/**
 * Data base methods
 */
public class DB {

    /**
     * Connection
     */
    private Connection connection;

    /**
     * Statrmrnt
     */
    private Statement stmt;

    /**
     * Prepared Statement
     */
    private PreparedStatement pstmt;

    /**
     * Constructor
     */
    public DB(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }

        try{
            connection = DriverManager.getConnection(
                    ConfigDataBase.getUrl(),
                    ConfigDataBase.getUser(),
                    ConfigDataBase.getPassword());
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Set auto commit
     * @param val
     */
    public final void setAutoCommit(boolean val){
        try {
            connection.setAutoCommit(val);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Get connection
     * @return
     */
    public final Connection getConnection(){
        return connection;
    }

    /**
     * Creating and getting statement
     */
    public Statement getStatement(){
        if(stmt == null){
            try {
                stmt = connection.createStatement();
            } catch (SQLException sqlEx) {
                sqlEx.printStackTrace();
            }
        }

        return stmt;
    }

    /**
     * Creating and getting prepared statement
     * @param tablePreparedStatement
     * @param query
     * @return
     */
    private PreparedStatement getPreparedStatement(TablePreparedStatement tablePreparedStatement, String query){
        try{
            pstmt = connection.prepareStatement(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        if(pstmt != null){
            try {
                pstmt = tablePreparedStatement.setAttributes(pstmt);
            } catch (SQLException sqlEx) {
                sqlEx.printStackTrace();
            }
        }

        return pstmt;
    }

    /**
     * Create table
     * @param tableName
     * @param columns
     */
    public final void createTable(String tableName, TableColumn[] columns){
        Statement stmt = getStatement();
        String query = "CREATE TABLE IF NOT EXISTS " + tableName + " (";

        for(int i=0; i<columns.length; i++){
            query += columns[i].name;
            query += " " + columns[i].type;
            query += (columns[i].primary_key) ? " PRIMARY KEY" : "";
            query += (columns[i].not_null) ? " NOT NULL" : "";
            query += (i+1<columns.length) ? "," : "";
        }

        query += ")";

        try {
            stmt.executeUpdate(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Get insert query
     * @param tableName
     * @param colVal
     * @return
     */
    private final String getInsertQquery(String tableName, Map<String, String> colVal){
        String query = "INSERT INTO " + tableName;
        String queryColumns = "";
        String queryValues = "";

        for(Map.Entry<String, String> colKeyVal : colVal.entrySet()){
            queryColumns += colKeyVal.getKey();
            queryColumns += ",";

            queryValues += "'" + colKeyVal.getValue() + "'";
            queryValues += ",";
        }

        query += " (" + queryColumns + ") VALUES (" + queryValues + ")";
        query = query.replace(",)", ")");

        return query;
    }

    /**
     * Insert in table
     * @param tableName
     * @param colVal
     */
    public final void insert(String tableName, Map<String, String> colVal){
        if(!tableIsExist(tableName)) return;

        Statement stmt = getStatement();
        String query = getInsertQquery(tableName, colVal);

        try {
            stmt.executeUpdate(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Insert in table with tablePreparedStatement
     * @param tableName
     * @param colVal
     * @param tablePreparedStatement
     */
    public final void insert(String tableName, Map<String, String> colVal, TablePreparedStatement tablePreparedStatement){
        if(!tableIsExist(tableName)) return;

        String query = getInsertQquery(tableName, colVal).replace("'?'", "?");
        PreparedStatement pstmt = getPreparedStatement(tablePreparedStatement, query);

        try {
            pstmt.executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Drop table
     * @param tableName
     */
    public final void dropTable(String tableName){
        Statement stmt = getStatement();
        try {
            stmt.executeUpdate("DROP TABLE IF EXISTS " + tableName);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Get update query
     * @param tableName
     * @param colVal
     * @param where
     * @return
     */
    private final String getUpdateQuery(String tableName, Map<String, String> colVal, String where){
        String query = "UPDATE " + tableName + " SET ";

        for (Map.Entry<String, String> column: colVal.entrySet()){
            query += column.getKey() + "='" + column.getValue() + "',";
        }

        query += ",";
        query = query.replace(",,", "");

        if(where != null && where != ""){
            query += " WHERE " + where;
        }

        return query;
    }

    /**
     * Update
     * @param tableName
     * @param colVal
     * @param where
     */
    public final void update(String tableName, Map<String, String> colVal, String where){
        if(!tableIsExist(tableName)) return;

        Statement stmt = getStatement();
        String query = getUpdateQuery(tableName, colVal, where);

        try {
            stmt.executeUpdate(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Update with tablePreparedStatement
     * @param tableName
     * @param colVal
     * @param where
     * @param tablePreparedStatement
     */
    public final void update(String tableName, Map<String, String> colVal, String where, TablePreparedStatement tablePreparedStatement){
        if(!tableIsExist(tableName)) return;

        String query = getInsertQquery(tableName, colVal).replace("'?'", "?");
        PreparedStatement pstmt = getPreparedStatement(tablePreparedStatement, query);

        try {
            pstmt.executeUpdate();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Get delete query
     * @param tableName
     * @param where
     * @return
     */
    private final String getDeleteQuery(String tableName, String where){
        String query = "DELETE FROM " + tableName;

        if(where != null && where != ""){
            query += " WHERE " + where;
        }

        return query;
    }

    /**
     * Delete
     * @param tableName
     * @param where
     */
    public final void delete(String tableName, String where){
        if(!tableIsExist(tableName)) return;

        Statement stmt = getStatement();
        String query = getDeleteQuery(tableName, where);

        try {
            stmt.executeUpdate(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Get select query
     * @param tableName
     * @param columns
     * @param where
     * @return
     */
    private final String getSelectQuery(String tableName, String[] columns, String where){
        String query = "SELECT ";

        for(int i=0; i<columns.length; i++){
            query += columns[i] + ",";
        }

        query += ",";
        query = query.replace(",,", "");

        query += " FROM " + tableName;

        if(where != null && where != ""){
            query += " WHERE " + where;
        }

        return query;
    }

    /**
     * Get select query
     * @param tableName
     * @param columns
     * @param where
     * @return
     */
    private final String getSelectQuery(String tableName, String columns, String where){
        String query = "SELECT " + columns + " FROM " + tableName;

        if(where != null && where != ""){
            query += " WHERE " + where;
        }

        return query;
    }

    /**
     * Get select prepared statement result set
     * @param tablePreparedStatement
     * @param query
     * @return
     */
    private final ResultSet getSelectPstmtResultSet(TablePreparedStatement tablePreparedStatement, String query){
        ResultSet resultSet = null;
        PreparedStatement pstmt = getPreparedStatement(tablePreparedStatement, query);

        try {
            resultSet = pstmt.executeQuery();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return resultSet;
    }

    /**
     * Get select result set
     * @param query
     * @return
     */
    private final ResultSet getSelectStmtResultSet(String query){
        Statement stmt = getStatement();
        ResultSet resultSet = null;

        try {
            resultSet = stmt.executeQuery(query);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return resultSet;
    }

    /**
     * Select
     * @param tableName
     * @param columns
     * @param where
     * @return
     */
    public final ResultSet select(String tableName, String[] columns, String where){
        if(!tableIsExist(tableName)) return null;
        return getSelectStmtResultSet(getSelectQuery(tableName, columns, where));
    }

    /**
     * Select
     * @param tableName
     * @param columns
     * @param where
     * @return
     */
    public final ResultSet select(String tableName, String columns, String where){
        if(!tableIsExist(tableName)) return null;
        return getSelectStmtResultSet(getSelectQuery(tableName, columns, where));
    }

    /**
     * Select with tablePreparedStatement
     * @param tableName
     * @param columns
     * @param where
     * @param tablePreparedStatement
     * @return
     */
    public final ResultSet select(String tableName, String[] columns, String where, TablePreparedStatement tablePreparedStatement){
        if(!tableIsExist(tableName)) return null;
        return getSelectPstmtResultSet(tablePreparedStatement, getSelectQuery(tableName, columns, where).replace("'?'", "?"));
    }

    /**
     * Select with tablePreparedStatement
     * @param tableName
     * @param columns
     * @param where
     * @param tablePreparedStatement
     * @return
     */
    public final ResultSet select(String tableName, String columns, String where, TablePreparedStatement tablePreparedStatement){
        if(!tableIsExist(tableName)) return null;
        return getSelectPstmtResultSet(tablePreparedStatement, getSelectQuery(tableName, columns, where).replace("'?'", "?"));
    }

    /**
     * Check table exist
     * @param tableName
     * @return
     */
    private boolean tableIsExist(String tableName){
        try{
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tablesResultSet = metaData.getTables(null, null, tableName, new String[]{"TABLE"});
            if (tablesResultSet.next()) return true;
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return false;
    }

    /**
     * Commit query
     */
    public final void commit(){
        try {
            connection.commit();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    /**
     * Close connection
     */
    public final void close(){
        try{
            if(stmt != null){
                stmt.close();
            }
            if(pstmt != null){
                pstmt.close();
            }
            connection.close();
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
