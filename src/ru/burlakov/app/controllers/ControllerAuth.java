package ru.burlakov.app.controllers;

import ru.burlakov.core.Controller;
import ru.burlakov.app.models.ModelChat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


/**
 * Auth page controller
 */
public class ControllerAuth extends Controller {

    /**
     * Action page (default | registration);
     */
    public String action;

    /**
     * Default constructor
     */
    public ControllerAuth(){
        this.action = "default";
    }

    /**
     * Constructor with action parameter
     * @param action
     */
    public ControllerAuth(String action){
        this.action = action;
    }

    /**
     * Get query
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        Map<String, Cookie> cookieMap = ModelChat.getCookieMap(req.getCookies());

        if(cookieMap != null){
            if(cookieMap.get("login") != null && cookieMap.get("password") != null){
                String login = cookieMap.get("login").getValue();
                String password = cookieMap.get("password").getValue();

                if(ModelChat.isRegisteredUser(login, password)) res.sendRedirect("/");

            }
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/templates/auth/index.jsp");
        req.setAttribute("action", action);
        dispatcher.forward(req, res);
    }
}
