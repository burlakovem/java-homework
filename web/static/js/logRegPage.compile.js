(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

/*
* This js file contains logReg page scripts
*/
$(document).ready(function () {
    var SignInForm = new window.site.SignInForm({
        loginField: '#formSignIn .formScript-loginField',
        loginInput: '#formSignIn .formScript-loginInput',
        passwordField: '#formSignIn .formScript-passwordField',
        passwordInput: '#formSignIn .formScript-passwordInput',
        errorField: '#formSignIn .formScript-errorField',
        submit: '#formSignIn .formScript-submit',
        focusClass: 'regInput-focus',
        translates: window.TRANSLATES.ru
    });

    var RegisterForm = new window.site.RegisterForm({
        loginField: '#formRegister .formScript-loginField',
        loginInput: '#formRegister .formScript-loginInput',
        loginError: '#formRegister .formScript-loginError',
        passwordField: '#formRegister .formScript-passwordField',
        passwordInput: '#formRegister .formScript-passwordInput',
        submit: '#formRegister .formScript-submit',
        focusClass: 'regInput-focus',
        errorClass: 'regInput-error',
        validClass: 'regInput-valid',
        translates: window.TRANSLATES.ru
    });

    var formSignIn = $('#formSignIn');
    var formRegister = $('#formRegister');

    function hideRegister() {
        formSignIn.css({
            'position': '',
            'opacity': 1
        });
        formRegister.css('position', 'absolute');

        formSignIn.animate({ left: 0 });
        formRegister.animate({ left: '100vw' }, {
            done: function done() {
                formRegister.css('display', 'none');
            }
        });

        window.onpopstate = function (e) {
            e.preventDefault();
            event.preventDefault();
            showRegister();
        };
    }

    function showRegister() {
        formSignIn.css('position', 'absolute');
        formRegister.css({
            'position': '',
            'display': '',
            'opacity': 1
        });

        formSignIn.animate({ left: '-100vw' });
        formRegister.animate({ left: 0 });

        if (document.location.pathname != '/auth/registration') {

            history.pushState(null, null, '/auth/registration');
        }

        window.onpopstate = function (e) {
            e.preventDefault();
            event.preventDefault();
            hideRegister();
        };
    }

    if (ACTION == 'registration') {
        showRegister();
    } else {
        hideRegister();
    }

    $('#openRegister').click(function (e) {
        e.preventDefault();
        showRegister();
    });
});

},{}]},{},[1]);
