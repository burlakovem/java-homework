package ru.burlakov.core.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * Table prepared statement methods
 */
abstract public class TablePreparedStatement {

    /**
     * Replace this method to set your options
     * @param pstmt
     * @return
     */
    public PreparedStatement setAttributes(PreparedStatement pstmt) throws SQLException {
        return pstmt;
    }
}
