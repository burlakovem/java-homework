package ru.burlakov.core;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Default constructor
 */
abstract public class Controller extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {}
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {}
}
