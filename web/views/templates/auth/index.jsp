<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'/>
    <title>start</title>
    <link rel='stylesheet' href='/static/css/main.css'/>
    <link rel='stylesheet' href='/static/css/logReg.css'/>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="body relative default-font">
<div class="decor-bg">
    <div class="decor-bg__img"></div>
</div>
<h1 class="home-logo home-logo-position relative"><img class="home-logo__img" src="/static/img/logo.png" alt="just-chat"/></h1>
<div class="formContainer relative">
    <div id="formSignIn" class="formContainer__content relative" style="opacity: 0">
        <form class="formHome formHome-positionCenter">
            <p class="formErrorBlock regInput-positionForm formScript-errorField">Ужасная ошибка</p>
            <div class="regInput regInput-positionForm relative formScript-loginField">
                <input class="regInput__input formScript-loginInput" type="text" placeholder="Логин"/>
                <svg version="1.1" class="regInput__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 47 47.3" style="enable-background:new 0 0 47 47.3;" xml:space="preserve">
							<circle class="st0" cx="23.5" cy="23.8" r="21.1"/>
                    <circle class="st0" cx="23.5" cy="19.7" r="5.8"/>
                    <path class="st0" d="M38.8,38.8c0-4-6.9-7.2-15.4-7.2S8.1,34.8,8.1,38.8"/>
						</svg>
                <span class="regInput__error formScript-loginError">error</span>
            </div>
            <div class="regInput relative formScript-passwordField">
                <input class="regInput__input formScript-passwordInput" type="text" placeholder="Пароль"/>
                <span class="regInput__error passwordError">error</span>
                <svg version="1.1" class="regInput__icon regInput__icon-password" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 42.6 53.2" style="enable-background:new 0 0 42.6 53.2;" xml:space="preserve">
							<circle class="st0" cx="21.4" cy="32.2" r="18.6"/>
                    <circle class="st1" cx="21.4" cy="32.2" r="4.2"/>
                    <path class="st1" d="M11.5,16.4v-4.1c0-1.5,0.3-3,0.9-4.3c1.2-2.6,3.8-6,9-6c5,0,7.6,3.1,9,5.6c0.8,1.5,1.2,3.3,1.2,5v4.1"/>
							</svg>
            </div>
            <input class="regSubmit regSubmit-position formScript-submit" value="Войти" type="submit"/>
            <div class="userHelper userHelper-positionCenter userHelper-positionRegPag relative">
                <a href="/registration" id="openRegister" class="userHelper__text userHelper__text-left" >Создать аккаунт</a>
                <a href="#" class="userHelper__text userHelper__text-right" >Нужна помощь?</a>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>

    <div id="formRegister" class="formContainer__content relative" style="opacity: 0">
        <form class="formHome formHome-hideRight formHome-positionCenter">
            <div class="regInput regInput-positionForm relative formScript-loginField">
                <input class="regInput__input formScript-loginInput" type="text" placeholder="Логин"/>
                <svg version="1.1" class="regInput__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 47 47.3" style="enable-background:new 0 0 47 47.3;" xml:space="preserve">
							<circle class="st0" cx="23.5" cy="23.8" r="21.1"/>
                    <circle class="st0" cx="23.5" cy="19.7" r="5.8"/>
                    <path class="st0" d="M38.8,38.8c0-4-6.9-7.2-15.4-7.2S8.1,34.8,8.1,38.8"/>
						</svg>
                <span class="regInput__error formScript-loginError">error</span>
            </div>
            <div class="regInput relative formScript-passwordField">
                <input class="regInput__input formScript-passwordInput" type="text" placeholder="Пароль"/>
                <span class="regInput__error passwordError">error</span>
                <svg version="1.1" class="regInput__icon regInput__icon-password" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 42.6 53.2" style="enable-background:new 0 0 42.6 53.2;" xml:space="preserve">
							<circle class="st0" cx="21.4" cy="32.2" r="18.6"/>
                    <circle class="st1" cx="21.4" cy="32.2" r="4.2"/>
                    <path class="st1" d="M11.5,16.4v-4.1c0-1.5,0.3-3,0.9-4.3c1.2-2.6,3.8-6,9-6c5,0,7.6,3.1,9,5.6c0.8,1.5,1.2,3.3,1.2,5v4.1"/>
							</svg>
            </div>
            <input class="regSubmit regSubmit-position formScript-submit" value="Регистрация" type="submit"/>
        </form>
    </div>
</div>
<script>
    var ACTION = '<%= request.getAttribute("action") %>';
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src='/static/js/api.compile.min.js'></script>
<script src='/static/js/main.compile.min.js'></script>
<script src='/static/js/logRegPage.compile.min.js'></script>
</body>
</html>