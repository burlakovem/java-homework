package ru.burlakov.core.db;

import java.sql.ResultSet;
import java.util.Map;
import java.util.Arrays;


/**
 * Abstract data base table class
 */
abstract public class Table {

    /**
     * Table name
     */
    private String tableName;

    /**
     * Table columns
     */
    private TableColumn[] columns;

    /**
     * Columns names
     */
    private String[] colNames;

    /**
     * Date base
     */
    private DB DB = new DB();

    /**
     * Create table
     */
    protected void createTable(String tableName, TableColumn[] columns){
        DB.createTable(tableName, columns);

        colNames = new String[columns.length];

        for(int i=0; i<columns.length; i++){
            colNames[i] = columns[i].name;
        }

        this.tableName = tableName;
        this.columns = columns;
    }

    /**
     * Set auto commit
     * @param val
     */
    public final void setAutoCommit(boolean val){
        DB.setAutoCommit(val);
    }

    /**
     * Check columns in table
     * @param colVal
     * @return
     */
    private final boolean checkColumnsInTable(Map<String, String> colVal){
        for (Map.Entry<String, String> column: colVal.entrySet()){
            if( Arrays.asList(colNames).indexOf(column.getKey()) == -1){
                System.out.println("Error: Column '" + column.getKey() + "' is not in the table '" + tableName + "'!");
                return false;
            }
        }
        return true;
    }

    /**
     * Check columns in table
     * @param col
     * @return
     */
    private final boolean checkColumnsInTable(String[] col){
        for(int i=0; i<col.length; i++){
            if( Arrays.asList(colNames).indexOf(col[i]) == -1){
                System.out.println("Error: Column '" + col[i] + "' is not in the table '" + tableName + "'!");
                return false;
            }
        }
        return true;
    }

    /**
     * Insert in table
     * @param colVal
     */
    public final void insert(Map<String, String> colVal){
        if(!checkColumnsInTable(colVal)) return;
        DB.insert(tableName, colVal);
    }

    /**
     * Insert in table with TablePreparedStatement parameter
     * @param colVal
     * @param tablePreparedStatement
     */
    public final void insert(Map<String, String> colVal, TablePreparedStatement tablePreparedStatement){
        if(!checkColumnsInTable(colVal)) return;
        DB.insert(tableName, colVal, tablePreparedStatement);
    }

    /**
     * Drop table
     */
    public final void dropTable(){
        DB.dropTable(tableName);
    }

    /**
     * Update
     * @param colVal
     * @param where
     */
    public final void update(Map<String, String> colVal, String where){
        if(!checkColumnsInTable(colVal)) return;
        DB.update(tableName, colVal, where);
    }

    /**
     * Update with tablePreparedStatement
     * @param colVal
     * @param where
     * @param tablePreparedStatement
     */
    public final void update(Map<String, String> colVal, String where, TablePreparedStatement tablePreparedStatement) {
        if(!checkColumnsInTable(colVal)) return;
        DB.update(tableName, colVal, where, tablePreparedStatement);
    }

    /**
     * Delete
     * @param where
     */
    public final void delete(String where) {
        DB.delete(tableName, where);
    }

    /**
     * Select
     * @param columns
     * @param where
     * @return
     */
    public final ResultSet select(String[] columns, String where) {
        if(!checkColumnsInTable(columns)) return null;
        return DB.select(tableName, columns, where);
    }

    /**
     * Select
     * @param columns
     * @param where
     * @return
     */
    public final ResultSet select(String columns, String where) {
        return DB.select(tableName, columns, where);
    }

    /**
     * Select
     * @param columns
     * @param where
     * @param tablePreparedStatement
     * @return
     */
    public final ResultSet select(String[] columns, String where, TablePreparedStatement tablePreparedStatement) {
        if(!checkColumnsInTable(columns)) return null;
        return DB.select(tableName, columns, where, tablePreparedStatement);
    }

    /**
     * Select
     * @param columns
     * @param where
     * @param tablePreparedStatement
     * @return
     */
    public final ResultSet select(String columns, String where, TablePreparedStatement tablePreparedStatement) {
        return DB.select(tableName, columns, where, tablePreparedStatement);
    }

    /**
     * Commit
     */
    public final void commit(){
        DB.commit();
    }

    /**
     * Close
     */
    public final void close(){
        DB.close();
    }
}
