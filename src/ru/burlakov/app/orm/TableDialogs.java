package ru.burlakov.app.orm;

import ru.burlakov.core.db.Table;
import ru.burlakov.core.db.TableColumn;
import ru.burlakov.core.db.TablePreparedStatement;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;


public class TableDialogs extends Table {

    /**
     * Constructor
     */
    public TableDialogs(){
        createTable("dialogs", new TableColumn[] {
                new TableColumn("id", "serial", true),
                new TableColumn("author_id", "int", false, true),
                new TableColumn("name", "varchar(50)", false, true),
                new TableColumn("date", "timestamp", false, true),
        });
    }

    /**
     * Set dialog
     * @param author_id
     * @param name
     */
    public final void setDialog(int author_id, String name){
        insert(new LinkedHashMap<String, String>() {{
            put("author_id", "?");
            put("name", "?");
            put("date", new Date().toString());
        }}, new TablePreparedStatement() {
            @Override
            public PreparedStatement setAttributes(PreparedStatement pstmt) throws SQLException {
                pstmt.setInt(1, author_id);
                pstmt.setString(2, name);
                return pstmt;
            }
        });
    }
}
