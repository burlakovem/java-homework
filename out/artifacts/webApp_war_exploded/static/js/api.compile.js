(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

window.api = {
	send: function send(uri, data) {
		var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function (data) {
			console.log(data);
		};
		var error = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (data) {
			console.log(data);
		};

		$.ajax({
			type: 'post',
			url: '/api' + uri,
			data: data,
			success: callback,
			error: error
		});
	},
	auth: function auth(login, password) {
		var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function (data) {
			console.log(data);
		};
		var error = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (data) {
			console.log(data);
		};

		this.send('/auth', {
			action: 'AUTHORIZATION',
			patameters: {
				login: login,
				password: password
			}
		}, callback, error);
	}
};

},{}]},{},[1]);
