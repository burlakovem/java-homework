package ru.burlakov.core;

import ru.burlakov.app.configs.ConfigRoutes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


/**
 * Router
 */
@WebServlet("/")
public class Router extends HttpServlet {

    /**
     * Config routes
     */
    private static ConfigRoutes configRoutes = new ConfigRoutes();

    /**
     * Routes
     */
    private static Map<String, Controller> routes = configRoutes.getRoutes();

    /**
     * Add route in route list
     * @param route
     * @param controller
     */
    public static void addRoute(String route, Controller controller){
        routes.put(readyRequestURI(route), controller);
    }

    /**
     * Ready link before using
     * @param requestURI
     * @return
     */
    public static String readyRequestURI(String requestURI){
        if( requestURI.charAt(requestURI.length() - 1) !=  '/' ){
            requestURI += "/";
        }
        return requestURI;
    }

    /**
     * Get query
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String uri = readyRequestURI(req.getRequestURI());
        Controller controller = routes.get(uri);

        if(controller != null){
            controller.doGet(req, res);
        }
    }

    /**
     * Get query
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String uri = readyRequestURI(req.getRequestURI());
        Controller controller = routes.get(uri);

        if(controller != null){
            controller.doPost(req, res);
        }
    }

}
