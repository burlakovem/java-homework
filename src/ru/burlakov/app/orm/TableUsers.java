package ru.burlakov.app.orm;

import ru.burlakov.core.db.Table;
import ru.burlakov.core.db.TableColumn;
import ru.burlakov.core.db.TablePreparedStatement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;


/**
 * Data base table Users
 */
public class TableUsers extends Table {

    /**
     * Constructor
     */
    public TableUsers(){
        this.createTable("users", new TableColumn[] {
            new TableColumn("id", "serial", true),
                    new TableColumn("name", "varchar(30)", false, true),
                    new TableColumn("user_id", "varchar(30)", false, true),
                    new TableColumn("login", "varchar(30)", false, true),
                    new TableColumn("password", "varchar(50)", false, true),
                    new TableColumn("date", "timestamp", false, true)
        });
    }

    /**
     * Set user in table
     * @param name
     * @param user_id
     * @param password
     */
    public final void setUser(String name, String user_id, String login, String password){
        insert(new LinkedHashMap<String, String>() {{
            put("name", "?");
            put("user_id", "?");
            put("login", "?");
            put("password", "?");
            put("date", new Date().toString());
        }}, new TablePreparedStatement() {
            @Override
            public PreparedStatement setAttributes(PreparedStatement pstmt) throws SQLException {
                pstmt.setString(1, name);
                pstmt.setString(2, user_id);
                pstmt.setString(3, login);
                pstmt.setString(4, password);
                return pstmt;
            }
        });
    }

    /**
     * Get all users
     * @return
     */
    public final ResultSet getUsers(){
        return select("*", "");
    }

    public final ResultSet getUserByLogPass(String login, String password){
        return select("*", "login = ? AND password = ?", new TablePreparedStatement() {
            @Override
            public PreparedStatement setAttributes(PreparedStatement pstmt) throws SQLException{
                pstmt.setString(1, login);
                pstmt.setString(2, password);
                return pstmt;
            }
        });
    }

}
