package ru.burlakov.app.models;

import ru.burlakov.core.Model;
import ru.burlakov.app.orm.TableUsers;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Model Chat
 */
public class ModelChat extends Model {
    public static final boolean isRegisteredUser(String login, String password){
        TableUsers tableUsers = new TableUsers();
        ResultSet resultSet = tableUsers.getUserByLogPass(login, password);

        try{
            if(resultSet.next()){
                tableUsers.close();
                return true;
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        tableUsers.close();

        return false;
    }
}
