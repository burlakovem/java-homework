package ru.burlakov.app.controllers;

import ru.burlakov.core.Controller;
import ru.burlakov.app.models.ModelChat;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


public class ControllerMain extends Controller {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res){
        Map<String, Cookie> cookieMap = ModelChat.getCookieMap(req.getCookies());

        if (cookieMap != null && cookieMap.get("login") != null && cookieMap.get("password") != null){
            String login = cookieMap.get("login").getValue();
            String password = cookieMap.get("password").getValue();

            if(!ModelChat.isRegisteredUser(login, password)){

                try{
                    res.getWriter().println("You logined!");
                } catch (IOException e){
                    e.printStackTrace();
                }

            }
        }

        try{
            res.sendRedirect("/auth");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
