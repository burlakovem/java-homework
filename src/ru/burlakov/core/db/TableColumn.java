package ru.burlakov.core.db;


/**
 * Column for data base table
 */
public class TableColumn {

    /**
     * Column name
     */
    public String name;

    /**
     * Column type
     */
    public String type;

    /**
     * Is primary key
     */
    public boolean primary_key = false;

    /**
     * Is not null
     */
    public boolean not_null = false;

    /**
     * Default constructor
     * @param name
     * @param type
     */
    public TableColumn(String name, String type){
        this.name = name;
        this.type = type;
    }

    /**
     * Constructor with primary_key parameter
     * @param name
     * @param type
     * @param primary_key
     */
    public TableColumn(String name, String type, Boolean primary_key){
        this.name = name;
        this.type = type;
        this.primary_key = primary_key;
    }

    /**
     * Constructor with all parameters
     * @param name
     * @param type
     * @param primary_key
     * @param not_null
     */
    public TableColumn(String name, String type, Boolean primary_key, Boolean not_null){
        this.name = name;
        this.type = type;
        this.primary_key = primary_key;
        this.not_null = not_null;
    }
}
