package ru.burlakov.core;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;


/**
 * Default model
 */
public class Model {
    /**
     * Get cookie map
     * @param req
     * @return
     */
    public static final Map<String, Cookie> getCookieMap(Cookie cookies[]){
        if(cookies == null) return null;
        return new HashMap<String, Cookie>(){{
            for (int i=0; i<cookies.length; i++){
                put(cookies[i].getName(), cookies[i]);
            }
        }};
    }
}
