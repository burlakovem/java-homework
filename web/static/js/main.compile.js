(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

var _logReg = require('./logReg');

window.TRANSLATES = {}; /*
                        * This js file contains methods for all pages
                        */

$.ajax({
    type: 'get',
    async: false,
    url: '/static/translates.json',
    success: function success(data) {
        window.TRANSLATES = data;
    },
    error: function error(e) {
        console.log(e);
    }
});

window.site = {
    SignInForm: _logReg.SignInForm,
    RegisterForm: _logReg.RegisterForm

};

},{"./logReg":5}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
 * Abstract class sign-in and register form
 */
var Form = function () {
    function Form() {
        _classCallCheck(this, Form);
    }

    /*
    * Returned object
    * Returned parameters:
    * valid - true/false - bool
    * type - error type - string
    */


    _createClass(Form, [{
        key: 'checkLogin',
        value: function checkLogin(str) {
            var obj = {
                valid: false,
                type: ''
            };
            if (str.length < 5) {
                obj.type = 'login-length';
                return obj;
            }
            if (str.replace(/\d/g, '') == '') {
                obj.type = 'login-note-words';
                return obj;
            }
            if (str.replace(/\w/g, '') != '') {
                obj.type = 'login-using-spec-symbol-cyrillic';
                return obj;
            }
            obj.valid = true;
            return obj;
        }

        /*
         * Returned object
         * Returned parameters:
         * valid - true/false - bool
         * type - error type - string
         */

    }, {
        key: 'checkPassword',
        value: function checkPassword(str) {
            var obj = {
                valid: false,
                type: ''
            };
            if (str.length < 6) {
                obj.type = 'password-length';
                return obj;
            }
            var matchDigit = str.match(/\d/g);
            if (!matchDigit || matchDigit.length < 5) {
                obj.type = 'password-length-digit';
                return obj;
            }
            if (str.replace(/\w/g, '') != '') {
                obj.type = 'password-using-spec-symbol-cyrillic';
                return obj;
            }
            obj.valid = true;
            return obj;
        }
    }]);

    return Form;
}();

exports.default = Form;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Form2 = require('./Form');

var _Form3 = _interopRequireDefault(_Form2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*
 *
 * Sign-in class
 * Take parameters
 *
 * REQUIRED:
 * loginField - selector for login field
 * loginInput - selector for login input
 * loginError - selector for login error container
 *
 * passwordField - selector for password field
 * passwordInput - selector for password input
 * passwordError - selector for password error container
 *
 * submit - selector for submit btn
 * translates - translate - object
 *
 *
 * NOT REQUIRED:
 * focusClass - class for focus field
 * errorClass - class for error field
 * validClass - class for valid field
 *
*/

var RegisterForm = function (_Form) {
    _inherits(RegisterForm, _Form);

    function RegisterForm(props) {
        _classCallCheck(this, RegisterForm);

        var _this = _possibleConstructorReturn(this, (RegisterForm.__proto__ || Object.getPrototypeOf(RegisterForm)).call(this, props));

        var self = _this;
        self.translates = props.translates || [];
        self.loginField = $(props.loginField);
        self.loginInput = $(props.loginInput);
        self.loginError = $(props.loginError);

        self.passwordField = $(props.passwordField);
        self.passwordInput = $(props.passwordInput);
        self.passwordError = $(props.passwordError);

        self.focusClass = props.focusClass || 'focus';
        self.errorClass = props.errorClass || 'error';
        self.validClass = props.validClass || 'valid';

        self.loginInput.focus(function (e) {
            self.loginField.addClass(self.focusClass);
        });

        self.loginInput.focusout(function (e) {
            self.loginField.removeClass(self.focusClass);
        });

        self.passwordInput.focus(function (e) {
            self.passwordField.addClass(self.focusClass);
        });

        self.passwordInput.focusout(function (e) {
            self.passwordField.removeClass(self.focusClass);
        });

        self.loginInput.keyup(function (e) {
            var this_ = $(this);
            if (this_.val() != '') {
                var status = self.checkLogin(this_.val());
                if (!status.valid) {
                    return self.showloginError(status.type);
                } else {
                    return self.loginValid();
                }
            }
            return self.hideloginError();
        });

        self.passwordInput.keyup(function (e) {
            var this_ = $(this);
            if (this_.val() != '') {
                self.passwordField.addClass(self.errorClass);
            } else {
                self.passwordField.removeClass(self.errorClass);
            }
        });
        return _this;
    }

    _createClass(RegisterForm, [{
        key: 'showLoginError',
        value: function showLoginError(str) {
            this.loginNotValid();
            if (this.translates != []) {
                this.loginError.html(this.translates.error[str]);
            } else {
                this.loginError.html('Ужасная ошибка!');
            }
            this.loginError.slideDown();
            this.loginField.addClass(this.errorClass);
        }
    }, {
        key: 'hideLoginError',
        value: function hideLoginError() {
            this.loginError.slideUp();
            this.loginField.removeClass(this.errorClass);
        }
    }, {
        key: 'LoginValid',
        value: function LoginValid() {
            this.hideloginError();
            this.loginField.addClass(this.validClass);
        }
    }, {
        key: 'LoginNotValid',
        value: function LoginNotValid() {
            this.loginField.removeClass(this.validClass);
        }
    }]);

    return RegisterForm;
}(_Form3.default);

exports.default = RegisterForm;

},{"./Form":2}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Form2 = require('./Form');

var _Form3 = _interopRequireDefault(_Form2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*
 *
 * Sign-in class
 * Take parameters
 *
 * REQUIRED:
 * loginField - selector for login field
 * loginInput - selector for login input
 * loginError - selector for login error container
 *
 * passwordField - selector for password field
 * passwordInput - selector for password input
 * passwordError - selector for password error container
 *
 * submit - selector for submit btn
 * errorField - selector for field error
 * translates - translate - object
 *
 * NOT REQUIRED:
 * focusClass - class for focus field
 *
 */
var SignInForm = function (_Form) {
    _inherits(SignInForm, _Form);

    function SignInForm(props) {
        _classCallCheck(this, SignInForm);

        var _this = _possibleConstructorReturn(this, (SignInForm.__proto__ || Object.getPrototypeOf(SignInForm)).call(this, props));

        var self = _this;
        self.focusClass = props.focusClass || 'focus';
        self.translates = props.translates || [];
        self.loginField = $(props.loginField);
        self.loginInput = $(props.loginInput);
        self.passwordField = $(props.passwordField);
        self.passwordInput = $(props.passwordInput);
        self.submit = $(props.submit);
        self.errorField = $(props.errorField) || 'error';

        self.loginInput.focus(function (e) {
            self.loginField.addClass(self.focusClass);
        });

        self.loginInput.focusout(function (e) {
            self.loginField.removeClass(self.focusClass);
        });

        self.passwordInput.focus(function (e) {
            self.passwordField.addClass(self.focusClass);
        });

        self.passwordInput.focusout(function (e) {
            self.passwordField.removeClass(self.focusClass);
        });

        self.submit.click(function (e) {
            e.preventDefault();

            var validStatus = self.checkLogin(self.loginInput.val());
            if (!validStatus.valid) {
                return self.showError(validStatus.type);
            }

            var validStatus = self.checkPassword(self.passwordInput.val());
            if (!validStatus.valid) {
                return self.showError(validStatus.type);
            }

            self.hideError();
            self.send();
        });
        return _this;
    }

    _createClass(SignInForm, [{
        key: 'showError',
        value: function showError(str) {
            if (this.translates != []) {
                var error = this.translates.error['valid-login-or-password'];
            } else {
                var error = 'Ужасная ошибка!';
            }
            this.errorField.html(error);
            this.errorField.slideDown();
        }
    }, {
        key: 'hideError',
        value: function hideError() {
            this.errorField.slideUp();
        }
    }, {
        key: 'send',
        value: function send() {
            var self = this;
            window.api.auth(self.loginInput.val(), self.passwordInput.val(), function (data) {
                var res = JSON.parse(data);
                if (res.type == 'ok') {
                    document.location.href = res.parameters.refirectUri;
                } else {
                    console.log(res.more);
                }
            }, function (data) {
                console.log(data);
            });
        }
    }]);

    return SignInForm;
}(_Form3.default);

exports.default = SignInForm;

},{"./Form":2}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RegisterForm = exports.SignInForm = undefined;

var _SignInForm = require('./SignInForm');

var _SignInForm2 = _interopRequireDefault(_SignInForm);

var _RegisterForm = require('./RegisterForm');

var _RegisterForm2 = _interopRequireDefault(_RegisterForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.SignInForm = _SignInForm2.default;
exports.RegisterForm = _RegisterForm2.default;

},{"./RegisterForm":3,"./SignInForm":4}]},{},[1]);
