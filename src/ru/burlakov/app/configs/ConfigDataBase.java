package ru.burlakov.app.configs;


/**
 * Config for data base connection
 */
public class ConfigDataBase {

    /**
     * Data base url
     */
    private static final String url = "jdbc:postgresql://localhost:5432/postgres";

    /**
     * Data base username
     */
    private static final String user = "postgres";

    /**
     * Data base password
     */
    private static final String password = "1";

    /**
     * Getting url
     * @return
     */
    public static String getUrl(){
        return url;
    }

    /**
     * Getting user
     * @return
     */
    public static String getUser(){
        return user;
    }

    /**
     * Getting password
     * @return
     */
    public static String getPassword(){
        return password;
    }
}
